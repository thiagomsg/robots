package tg;

import robocode.*;
import static robocode.util.Utils.normalRelativeAngleDegrees;

public class GunHandle {
	AdvancedRobot bot;
	final static double FIREPOWER = 3;
	boolean fired = false;

	public GunHandle(AdvancedRobot bot) {
		this.bot = bot;
	}

	public void updateAim(ScannedRobotEvent target) {

		if (target != null) {

			double targetAngle = (bot.getHeading() + target.getBearing());
			bot.setTurnGunRight(normalRelativeAngleDegrees(targetAngle - bot.getGunHeading()));


			if (target.getVelocity() == 0) {
				
				//Alvo parado => atire com precisão 
				if (bot.getGunHeat() == 0 && Math.abs(bot.getGunTurnRemaining()) < Math.abs(Virtuoso.MIN_DEVIATION)) {

					FireEnemy(FIREPOWER);

				}
			}else {
				
				//Alvo em movimento => simplesmente atire
				if(bot.getGunHeat() == 0) {
					
					FireEnemy(2.5);

				}
			}
		}
	}

	public void FireEnemy(double firePower) {

		bot.fire(FIREPOWER);

	}
}
