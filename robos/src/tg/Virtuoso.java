package tg;

import java.awt.Color;

import robocode.AdvancedRobot;
import robocode.RobotDeathEvent;
import robocode.ScannedRobotEvent;
import robocode.WinEvent;

public class Virtuoso extends AdvancedRobot {
	public static final double MIN_DEVIATION = 1E-12;
	// constante que controla a direção do scan, 1 = sentido horário
	final double scanDir = 1;
	RadarHandle radarHandle = new RadarHandle(this);
	Movement movement = new Movement(this);
	GunHandle gunHandle = new GunHandle(this);
	ScannedRobotEvent target;

	public void run() {

		initialize();

		while (true) {

			execute();
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		if (target == null || target.getName().equals(e.getName())) {

			target = e;
			
		}else if(e.getDistance() < target.getDistance() || target.getEnergy() <= 0) {
			target = e;
		}

		if (getVelocity() == 0 && getTurnRemaining() == 0
				&& Math.abs(getGunTurnRemaining()) < Math.abs(MIN_DEVIATION)) {
			gunHandle.updateAim(target);
		}

		if (getGunHeat() != 0 && Math.abs(target.getBearing()) != Math.abs(90) && getTurnRemaining() == 0) {
			movement.perpendicularMovement(target);
		}
		if (getGunHeat() != 0) {
			movement.detectFire(target);
		}

	}
	
	public void onRobotDeath(RobotDeathEvent robotDeathEvent) {
		
		//Se nosso alvo morreu, procure outro alvo
		if(robotDeathEvent.getName() == target.getName()) {
			target = null;
		}
	}

	public void onWin(WinEvent e) {
		//dancinha da vitória
		setTurnRight(180);
		setTurnLeft(360);
	}

	private void initialize() {
		setAdjustGunForRobotTurn(true);
		setAdjustRadarForGunTurn(true);
		// Pintar robo
		setBodyColor(Color.gray);
		setGunColor(Color.red);
		setRadarColor(Color.yellow);
		out.println("xy : " + getBattleFieldWidth() + " x " + getBattleFieldHeight());

		// movimento de início de round
		radarHandle.radarSweep(scanDir);
		movement.goBorder();

	}

}
