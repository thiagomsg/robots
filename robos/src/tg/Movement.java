package tg;

import robocode.*;
import static robocode.util.Utils.normalRelativeAngleDegrees;

import java.util.Random;
import java.util.ArrayList;
import java.util.List;

public class Movement {
	List<Double> targetEnergyHistory = new ArrayList<>(2);
	AdvancedRobot bot;
	final static double MOV_AMT = 60;

	public Movement(AdvancedRobot bot) {
		this.bot = bot;

	}

	public void goBorder() {
		double bfHeight = bot.getBattleFieldHeight();
		double bfWidth = bot.getBattleFieldWidth();

		// calcular distância dos eixos em relação ao centro para escolher o menor
		// percurso
		double deltaX = bot.getX() - (bfWidth / 2);
		double deltaY = bot.getY() - (bfHeight / 2);

		// caso o robo não esteja no centro, tente otimizar o caminho até a borda
		if (bot.getX() != bfWidth / 2 || bot.getY() != bfHeight / 2) {

			// mapa quadrado
			if (bfHeight == bfWidth) {
				squareMapMovement(bfHeight, bfWidth, deltaX, deltaY);

				// mapa retangular largo
			} else if (bfWidth > bfHeight) {
				wideMapMovement(bfHeight, bfWidth, deltaX, deltaY);

				// mapa retangular alto
			} else {
				highMapMovement(bfHeight, bfWidth, deltaX, deltaY);
			}
			// caso robo no centro
		} else {
			// robo está no centro de um mapa quadrado vá arbitrariamente para sul
			if (bfHeight == bfWidth) {
				goSouth();
				// robo no centro de um mapa largo vá arbitrariamente para sul
			} else if (bfWidth > bfHeight) {
				goSouth();
				// robo no centro de um mapa alto, vá arbitrariamente para oeste
			} else {
				goWest();
			}
		}
	}

	private void highMapMovement(double bfHeight, double bfWidth, double deltaX, double deltaY) {
		double deltaBf;
		// calcular diferença entre largura e altura do mapa
		deltaBf = bfHeight - bfWidth;
		if (Math.abs(deltaY) > Math.abs(deltaX) + deltaBf || Math.abs(deltaY) == Math.abs(deltaX) + deltaBf) {
			// se mais próximo do sul, ir para sul
			if (deltaY > 0) {
				goSouth();
			} else {
				goNorth(bfHeight);
			}

		} else {
			// se mais proximo de leste, ir para leste
			if (deltaX > 0) {
				goEast(bfWidth);
			} else {
				goWest();
			}
		}
	}

	private void wideMapMovement(double bfHeight, double bfWidth, double deltaX, double deltaY) {
		double deltaBf;
		// calcular diferença entre largura e altura do mapa
		deltaBf = bfWidth - bfHeight;
		if (Math.abs(deltaY) + deltaBf > Math.abs(deltaX) || Math.abs(deltaY) + deltaBf == Math.abs(deltaX)) {
			// se mais próximo do sul, ir para sul
			if (deltaY < 0) {
				goSouth();
			} else {
				goNorth(bfHeight);
			}

		} else {
			// se mais proximo de leste, ir para leste
			if (deltaX > 0) {
				goEast(bfWidth);
			} else {
				goWest();
			}
		}
	}

	private void squareMapMovement(double bfHeight, double bfWidth, double deltaX, double deltaY) {
		// caso o robo esteja mais longe do centro verticalmente, verificar se é melhor
		// ir para a borda superior ou inferior
		// ou caso as distancias dos eixos em relação ao centro seja a mesma
		if (Math.abs(deltaY) > Math.abs(deltaX) || Math.abs(deltaY) == Math.abs(deltaX)) {
			// se mais próximo do sul, ir para sul
			if (deltaY > 0) {
				goSouth();
				// se mais proximo do norte, ir para norte
			} else {
				goNorth(bfHeight);
			}

		} else {
			// se mais proximo de leste, ir para leste
			if (deltaX > 0) {
				goEast(bfWidth);
			} else {
				// se mais proximo de oeste ir para oeste
				goWest();
			}
		}
	}

	private void goWest() {
		bot.turnRight(normalRelativeAngleDegrees(-90 - bot.getHeading()));
		bot.ahead(bot.getX() - 20);
		// FIXME provisorio
		if (bot.getY() > 0.5 * bot.getBattleFieldHeight()) {
			bot.turnRight(normalRelativeAngleDegrees(180 - bot.getHeading()));
		} else {
			bot.turnRight(normalRelativeAngleDegrees(0 - bot.getHeading()));
		}
	}

	private void goEast(double bfWidth) {
		bot.turnRight(normalRelativeAngleDegrees(90 - bot.getHeading()));
		bot.ahead(bfWidth - bot.getX() - 20);
		bot.turnLeft(normalRelativeAngleDegrees(180 - bot.getHeading()));
		// FIXME provisorio
		if (bot.getY() > 0.5 * bot.getBattleFieldHeight()) {
			bot.turnRight(normalRelativeAngleDegrees(180 - bot.getHeading()));
		} else {
			bot.turnRight(normalRelativeAngleDegrees(0 - bot.getHeading()));
		}
	}

	private void goNorth(double bfHeight) {
		bot.turnRight(normalRelativeAngleDegrees(0 - bot.getHeading()));
		bot.ahead(bfHeight - bot.getY() - 20);
		// FIXME provisorio
		if (bot.getX() > 0.5 * bot.getBattleFieldWidth()) {
			bot.turnRight(normalRelativeAngleDegrees(-90 - bot.getHeading()));
		} else {
			bot.turnRight(normalRelativeAngleDegrees(90 - bot.getHeading()));
		}
	}

	private void goSouth() {
		bot.out.println("Y: " + bot.getY());
		bot.turnRight(normalRelativeAngleDegrees(180 - bot.getHeading()));
		bot.ahead(bot.getY() - 20);
		// FIXME provisorio
		if (bot.getX() > 0.5 * bot.getBattleFieldWidth()) {
			bot.turnRight(normalRelativeAngleDegrees(-90 - bot.getHeading()));
		} else {
			bot.turnRight(normalRelativeAngleDegrees(90 - bot.getHeading()));
		}
	}

	public void perpendicularMovement(ScannedRobotEvent target) {
		// se robo estiver em uma quina aborte

		if (bot.getX() >= 0.75 * bot.getBattleFieldWidth() && bot.getY() >= 0.75 * bot.getBattleFieldHeight()
		 || bot.getY() >= 0.75 * bot.getBattleFieldHeight() && bot.getX() <= 0.25 * bot.getBattleFieldWidth()
		 || bot.getY() <= 0.25 * bot.getBattleFieldHeight() || bot.getX() <= 0.25 * bot.getBattleFieldWidth()
		 || bot.getX() >= 0.75 * bot.getBattleFieldWidth() && bot.getY() <= 0.25 * bot.getBattleFieldHeight()) {
			return;
		}

		if (target != null) {
			double tgtBearing = target.getBearing();

			if (tgtBearing == 90 || tgtBearing == -90) {
				return;
			}

			if (tgtBearing >= 0 && tgtBearing < 90) {
				bot.setTurnLeft(90 - tgtBearing);

			} else if (tgtBearing > 90 && tgtBearing > 180) {
				bot.setTurnRight(tgtBearing - 90);

			} else if (tgtBearing < 0 && tgtBearing > -90) {
				bot.setTurnRight(tgtBearing + 90);

			} else if (tgtBearing < -90 && tgtBearing >= -180) {
				bot.setTurnLeft(-90 - tgtBearing);
			}
		}
	}

	public void detectFire(ScannedRobotEvent target) {
		targetEnergyHistory.add(target.getEnergy());

		// abortar no primeiro tick
		if (targetEnergyHistory.size() < 2) {
			return;

		} else {
			double deltaEnergy = targetEnergyHistory.get(0) - targetEnergyHistory.get(1);
			targetEnergyHistory.set(0, targetEnergyHistory.get(1));
			targetEnergyHistory.remove(1);

			// inimigo disparou?
			
			bot.out.println("deltaEnergy: " + deltaEnergy);
			if (deltaEnergy > 0.1 && deltaEnergy < 3.1) {
				dodgeBullets(target);
			}
		}
	}

	private void dodgeBullets(ScannedRobotEvent target) {
		Random rd = new Random();
		// verificar se robo está em uma quina

		// se o robo está na quina superior direita
		if (bot.getX() >= 0.75 * bot.getBattleFieldWidth() && bot.getY() >= 0.75 * bot.getBattleFieldHeight()) {
			northeastCornerDodge(target, rd);

			// se o robo está na quina inferior direita
		} else if (bot.getX() >= 0.75 * bot.getBattleFieldWidth() && bot.getY() <= 0.25 * bot.getBattleFieldHeight()) {
			southeastCornerDodge(target, rd);

			// se o robo está na quina superior esquerda
		} else if (bot.getX() <= 0.25 * bot.getBattleFieldWidth() && bot.getY() >= 0.75 * bot.getBattleFieldHeight()) {
			northwestCornerDodge(target, rd);

			// se o robo está na quina inferior esquerda
		} else if (bot.getX() <= 0.25 * bot.getBattleFieldWidth() && bot.getY() <= 0.25 * bot.getBattleFieldHeight()) {
			southwestCornerDodge(target, rd);

			// se o robo nao esta em uma quina, verificar se está em uma borda

			// se o robo está na borda oeste
		} else if (bot.getX() <= 0.25 * bot.getBattleFieldWidth()) {
			westBorderDodge(target, rd);

			// se o robo esta na borda leste
		} else if (bot.getX() >= 0.75 * bot.getBattleFieldWidth()) {
			eastBorderDodge(target, rd);

			// se o robo está na borda norte
		} else if (bot.getY() >= 0.75 * bot.getBattleFieldHeight()) {
			northBorderDodge(target, rd);

			// se o robo esta na borda sul
		} else if (bot.getY() <= 0.25 * bot.getBattleFieldHeight()) {
			southBorderDodge(target, rd);

			// caso robo esteja relativamente centralizado no mapa

		} else {
			int side = rd.nextInt(2) + 1;
			switch (side) {

			case 1:
				bot.ahead(MOV_AMT);

				break;
			case 2:

				bot.back(MOV_AMT);

				break;

			default:
				break;
			}

		}

	}

	private void southBorderDodge(ScannedRobotEvent target, Random rd) {
		// se o robo estiver virado para sul
		if (bot.getHeading() < 270 && bot.getHeading() > 90) {
			bot.back(MOV_AMT);

			// se robo virado norte
		} else if (bot.getHeading() > 270 || (bot.getHeading() >= 0 && bot.getHeading() < 90)) {
			bot.ahead(MOV_AMT);

			// se o robo estiver virado para leste ou oeste
		} else {
			int side = rd.nextInt(2) + 1;
			switch (side) {

			case 1:
				bot.ahead(MOV_AMT);

				break;
			case 2:

				bot.back(MOV_AMT);

				break;

			default:
				break;
			}
		}

	}

	private void northBorderDodge(ScannedRobotEvent target, Random rd) {
		// se robo virado para norte
		if (bot.getHeading() > 270 || (bot.getHeading() >= 0 && bot.getHeading() < 90)) {
			bot.back(MOV_AMT);

			// se o robo estiver virado para sul
		} else if (bot.getHeading() < 270 && bot.getHeading() > 90) {
			bot.ahead(MOV_AMT);

			// se o robo estiver virado para leste ou oeste
		} else {
			int side = rd.nextInt(2) + 1;
			switch (side) {

			case 1:
				bot.ahead(MOV_AMT);

				break;
			case 2:

				bot.back(MOV_AMT);

				break;

			default:
				break;
			}
		}

	}

	private void eastBorderDodge(ScannedRobotEvent target, Random rd) {
		// robo virado para leste
		if (bot.getHeading() < 180 && bot.getHeading() > 0) {
			bot.back(MOV_AMT);

			// robo virado para oeste
		} else if (bot.getHeading() > 180) {
			bot.ahead(MOV_AMT);

			// robo virado para norte ou sul
		} else {
			int side = rd.nextInt(2) + 1;
			switch (side) {

			case 1:
				bot.ahead(MOV_AMT);

				break;
			case 2:

				bot.back(MOV_AMT);

				break;

			default:
				break;
			}

		}
	}

	private void westBorderDodge(ScannedRobotEvent target, Random rd) {
		// robo virado para oeste
		if (bot.getHeading() > 180) {
			bot.back(MOV_AMT);

			// virado para leste
		} else if (bot.getHeading() < 180 && bot.getHeading() > 0) {
			bot.ahead(MOV_AMT);

			// robo virado para norte ou sul
		} else {
			int side = rd.nextInt(2) + 1;
			switch (side) {

			case 1:
				bot.ahead(MOV_AMT);

				break;
			case 2:

				bot.back(MOV_AMT);

				break;

			default:
				break;
			}

		}

	}

	private void southwestCornerDodge(ScannedRobotEvent target, Random rd) {
		// casos em que o alvo está mais interno na quina

		// alvo interno e robo virado para noroeste
		if (bot.getHeading() > 270 && target.getBearing() < 0) {
			bot.setTurnLeft(normalRelativeAngleDegrees(-90 - bot.getHeading()));
			bot.back(MOV_AMT);

			// alvo interno e robo virado para sudeste
		} else if ((bot.getHeading() < 180 && bot.getHeading() > 90) && target.getBearing() > 0) {
			bot.setTurnLeft(normalRelativeAngleDegrees(90 - bot.getHeading()));
			bot.ahead(MOV_AMT);

			// alvo interno e robo virado para nordeste
		} else if (bot.getHeading() >= 0 && bot.getHeading() <= 90
				&& (target.getBearing() > 90 || target.getBearing() < -90)) {
			bot.ahead(MOV_AMT);

			// alvo interno e robo virado para sudoeste
		} else if (bot.getHeading() <= 270 && bot.getHeading() >= 180
				&& (target.getBearing() < 90 || target.getBearing() > -90)) {
			bot.back(MOV_AMT);

			// casos em que o alvo não está mais interno na quina

			// robo virado para Noroeste
		} else if (bot.getHeading() > 270 && target.getBearing() >= 0) {

			// se alvo mais para norte esquivar para leste
			if (target.getBearing() < 90) {
				bot.setTurnLeft(normalRelativeAngleDegrees(-90 - bot.getHeading()));
				bot.back(MOV_AMT);

				// se alvo mais para leste esquivar para norte
			} else {
				bot.setTurnRight(normalRelativeAngleDegrees(0 - bot.getHeading()));
				bot.ahead(MOV_AMT);
			}

			// robo virado para sudeste
		} else if ((bot.getHeading() < 180 && bot.getHeading() > 90) && target.getBearing() <= 0) {

			// se alvo mais para norte esquivar para leste
			if (target.getBearing() < -90) {
				bot.setTurnLeft(normalRelativeAngleDegrees(90 - bot.getHeading()));
				bot.ahead(MOV_AMT);

				// se alvo mais para leste esquivar para norte
			} else {
				bot.setTurnRight(normalRelativeAngleDegrees(-180 - bot.getHeading()));
				bot.back(MOV_AMT);
			}

			// robo virado para nordeste
		} else if (bot.getHeading() >= 0 && bot.getHeading() <= 90) {
			bot.ahead(MOV_AMT);

			// robo virado para sudoeste
		} else if (bot.getHeading() <= 270 && bot.getHeading() >= 180) {
			bot.back(MOV_AMT);

		}

	}

	private void northwestCornerDodge(ScannedRobotEvent target, Random rd) {

		// casos em que o alvo está mais interno na quina

		// alvo interno e robo virado para nordeste
		if (bot.getHeading() > 0 && bot.getHeading() < 90 && target.getBearing() < 0) {
			bot.setTurnRight(normalRelativeAngleDegrees(90 - bot.getHeading()));
			bot.ahead(MOV_AMT);

			// alvo interno e robo virado para sudoeste
		} else if (bot.getHeading() < 270 && bot.getHeading() > 180 && target.getBearing() > 0) {
			bot.setTurnLeft(normalRelativeAngleDegrees(180 - bot.getHeading()));
			bot.ahead(MOV_AMT);

			// alvo interno e robo virado para sudeste
		} else if ((bot.getHeading() <= 180 && bot.getHeading() >= 90)
				&& (target.getBearing() < -90 || target.getBearing() > 90)) {
			bot.ahead(MOV_AMT);

			// alvo interno e robo virado para noroeste
		} else if ((bot.getHeading() >= 270 || bot.getHeading() == 0)
				&& (target.getBearing() < 90 || target.getBearing() > -90)) {
			bot.back(MOV_AMT);

			// casos em que o alvo não está mais interno

			// robo virado pra nordeste
		} else if (bot.getHeading() > 0 && bot.getHeading() < 90 && target.getBearing() >= 0) {

			// se alvo mais para sul, desviar para leste
			if (target.getBearing() > 90) {
				bot.setTurnRight(normalRelativeAngleDegrees(90 - bot.getHeading()));
				bot.ahead(MOV_AMT);

				// alvo mais para leste, desviar para sul
			} else {
				bot.setTurnLeft(0 - bot.getHeading());
				bot.back(MOV_AMT);
			}

			// robo virado para sudoeste
		} else if (bot.getHeading() < 270 && bot.getHeading() > 180 && target.getBearing() <= 0) {

			// alvo mais para leste, desviar para sul
			if (target.getBearing() < -90) {
				bot.setTurnLeft(normalRelativeAngleDegrees(-180 - bot.getHeading()));
				bot.ahead(MOV_AMT);

				// alvo mais para sul, desviar para leste
			} else {
				bot.setTurnLeft(normalRelativeAngleDegrees(-90 - bot.getHeading()));
				bot.back(MOV_AMT);
			}

			// robo virado para noroeste
		} else if (bot.getHeading() >= 270 || bot.getHeading() == 0) {
			bot.back(MOV_AMT);

			// robo para sudeste
		} else if (bot.getHeading() <= 180 && bot.getHeading() >= 90) {
			bot.ahead(MOV_AMT);
		}

	}

	private void southeastCornerDodge(ScannedRobotEvent target, Random rd) {

		// casos em que o alvo está mais interno na quina

		// alvo interno e robo virado para nordeste
		if (bot.getHeading() > 0 && bot.getHeading() < 90 && target.getBearing() > 0) {
			bot.setTurnLeft(normalRelativeAngleDegrees(0 - bot.getHeading()));
			bot.ahead(MOV_AMT);

			// alvo interno e robo virado para sudoeste
		} else if (bot.getHeading() < 270 && bot.getHeading() > 180 && target.getBearing() < 0) {
			bot.setTurnRight(normalRelativeAngleDegrees(-90 - bot.getHeading()));
			bot.ahead(MOV_AMT);

			// alvo interno e robo virado para sudeste
		} else if ((bot.getHeading() <= 180 && bot.getHeading() >= 90)
				&& (target.getBearing() < 90 || target.getBearing() > -90)) {
			bot.back(MOV_AMT);

			// alvo interno e robo virado para noroeste
		} else if ((bot.getHeading() >= 270 || bot.getHeading() == 0)
				&& (target.getBearing() > 90 || target.getBearing() < -90)) {
			bot.ahead(MOV_AMT);

			// casos em que o alvo não está mais interno na quina

			// robo virado para nordeste
		} else if (bot.getHeading() >= 0 && bot.getHeading() <= 90 && target.getBearing() <= 0) {

			// caso alvo esteja mais para oeste, desviar para norte
			if (target.getBearing() <= -90) {
				bot.setTurnLeft(normalRelativeAngleDegrees(0 - bot.getHeading()));
				bot.ahead(MOV_AMT);

				// caso alvo esteja mais para norte, desviar para oeste
			} else {
				bot.setTurnLeft(normalRelativeAngleDegrees(90 - bot.getHeading()));
				bot.back(MOV_AMT);
			}

			// robo virado para sudoeste
		} else if (bot.getHeading() < 270 && bot.getHeading() > 180 && target.getBearing() >= 0) {

			// caso alvo mais para oeste, desviar para norte
			if (target.getBearing() < 90) {
				bot.setTurnLeft(normalRelativeAngleDegrees(-180 - bot.getHeading()));
				bot.back(MOV_AMT);

				// caso bot mais para norte, desviar para oeste
			} else {
				bot.setTurnRight(normalRelativeAngleDegrees(-90 - bot.getHeading()));
				bot.ahead(MOV_AMT);
			}

			// robo virado para noroeste
		} else if (bot.getHeading() >= 270 || bot.getHeading() == 0) {
			bot.ahead(MOV_AMT);

			// robo virado para sudeste
		} else if (bot.getHeading() >= 270 || bot.getHeading() == 0) {
			bot.back(MOV_AMT);
		}

	}

	private void northeastCornerDodge(ScannedRobotEvent target, Random rd) {
		// casos em que o alvo está mais interno na quina

		// alvo interno e robo virado para noroeste
		if (bot.getHeading() > 270 && target.getBearing() > 0) {
			bot.setTurnLeft(normalRelativeAngleDegrees(-90 - bot.getHeading()));
			bot.ahead(MOV_AMT);

			// alvo interno e robo virado para sudeste
		} else if ((bot.getHeading() < 180 && bot.getHeading() > 90) && target.getBearing() < 0) {
			bot.setTurnLeft(normalRelativeAngleDegrees(90 - bot.getHeading()));
			bot.back(MOV_AMT);

			// alvo interno e robo virado para nordeste
		} else if (bot.getHeading() >= 0 && bot.getHeading() <= 90
				&& (target.getBearing() < 90 || target.getBearing() > -90)) {
			bot.back(MOV_AMT);

			// alvo interno e robo virado para sudoeste
		} else if (bot.getHeading() <= 270 && bot.getHeading() >= 180
				&& (target.getBearing() > 90 || target.getBearing() < -90)) {
			bot.ahead(MOV_AMT);

			// casos em que o alvo não está mais interno na quina

			// robo virado para Noroeste
		} else if (bot.getHeading() > 270 && target.getBearing() <= 0) {

			// se alvo mais para sul esquivar para oeste
			if (target.getBearing() < -90) {
				bot.setTurnLeft(normalRelativeAngleDegrees(-90 - bot.getHeading()));
				bot.ahead(MOV_AMT);

				// se alvo mais para oeste esquivar para sul
			} else {
				bot.setTurnRight(normalRelativeAngleDegrees(0 - bot.getHeading()));
				bot.back(MOV_AMT);
			}

			// robo virado para sudeste
		} else if ((bot.getHeading() < 180 && bot.getHeading() > 90) && target.getBearing() >= 0) {

			// se alvo mais para sul esquivar para oeste
			if (target.getBearing() < 90) {
				bot.setTurnLeft(normalRelativeAngleDegrees(90 - bot.getHeading()));
				bot.back(MOV_AMT);

				// se alvo mais para oeste esquivar para sul
			} else {
				bot.setTurnRight(normalRelativeAngleDegrees(-180 - bot.getHeading()));
				bot.ahead(MOV_AMT);
			}

			// robo virado para nordeste
		} else if (bot.getHeading() >= 0 && bot.getHeading() <= 90) {
			bot.back(MOV_AMT);

			// robo virado para sudoeste
		} else if (bot.getHeading() <= 270 && bot.getHeading() >= 180) {
			bot.ahead(MOV_AMT);

		}
	}
}
