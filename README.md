Criação do robô Virtuoso para a Solutis Robot Arena 

O Virtuoso verifica se o oponente efetuou um disparo e tenta desviar utilizando uma trajetória imprevisível. Dispara na posição em que detectou o oponente.

Inicialmente o robô vai para uma das bordas do mapa (a mais próxima) para evitar o fogo cruzado no centro  
Em seguida o robô tenta se manter num ângulo perpendicular com o alvo para facilitar as manobras de esquiva  
Se for possível, o robô detecta o disparo do oponente e se move de maneira imprevisível.

-Ponto fraco: não prevê a posição futura do oponente.    
-Ponto forte: movimentação imprevisível.

Projeto inicial finalizado, mas pode receber atualizações no futuro. Uma das maiores dificuldades encontradas foi entender a API para extrair o máximo dela no gerenciamento dos recursos do robô (energia, cooling rate, ticks, etc). Apesar disso, o desenvolvimento tem sido uma experiência desafiadora e com muito aprendizado :)